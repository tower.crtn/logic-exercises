package com.ids.solutions.logic.exercises.util;

import com.ids.solutions.logic.exercises.constants.Constants;
import com.ids.solutions.logic.exercises.model.PrimeNumber;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Util {

  public static PrimeNumber getPrimeNumber(Integer number) {
    List<Integer> dividers = new ArrayList<>();

    for (int i = Constants.VALUE_INT_1; i <= number; i++) {
      if (number % i == Constants.VALUE_INT_0) {
        dividers.add(new Integer(i));
      }
    }
    return new PrimeNumber(number, dividers.size() > Constants.VALUE_INT_2 ? false : true,
        dividers);
  }

  public static int addUp(String num1, String num2) throws Exception {
    int num3 = Constants.VALUE_INT_0;
    try {
      num3 = Integer.parseInt(num1) + Integer.parseInt(num2);
    } catch (Exception err) {
      throw new Exception("Error in AddUp: " + err.getMessage());
    }
    return num3;
  }

  public static boolean complete(long seconds) throws InterruptedException {
    boolean flag = true;
    try {
      Thread.sleep(Duration.ofSeconds(seconds).toMinutes());
    } catch (InterruptedException err) {
      throw new InterruptedException("the request took too long");
    }
    return flag;
  }
  
  public static PrimeNumber process(long seconds, Integer number) throws InterruptedException {
    Util.complete(seconds);
    return Util.getPrimeNumber(number);
  }
  
  public static int getFibonacciNumber(int fibonacciPositional) {
    int fibVal1 = Constants.VALUE_INT_1;
    int fibVal2 = Constants.VALUE_INT_1;
    System.out.print(fibVal1 + ", " + fibVal2 + ", ");
    int fibonacciNumber = Constants.VALUE_INT_0;

    if (fibonacciPositional > Constants.VALUE_INT_0) {
      if (fibonacciPositional > Constants.VALUE_INT_2) {
        for (int i =
            Constants.VALUE_INT_0; i < (fibonacciPositional - Constants.VALUE_INT_2); i++) {
          fibonacciNumber = fibVal1 + fibVal2;
          System.out.print(fibonacciNumber + ", ");
          fibVal1 = fibVal2;
          fibVal2 = fibonacciNumber;
        }
      } else {
        fibonacciNumber = Constants.VALUE_INT_1;
      }
    } else {
      fibonacciNumber = Constants.VALUE_INT_0;
    }
    return fibonacciNumber;
  }
  
  public static int numericFactorial(int num) {
    int resp = Constants.VALUE_INT_1;
    while (num > Constants.VALUE_INT_0) {
      resp = resp * num;
      num--;
    }
    return resp;
  }
  
  public static void main(String[] args) {
    
  }
}
