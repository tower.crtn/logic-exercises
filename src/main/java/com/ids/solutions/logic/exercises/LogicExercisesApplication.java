package com.ids.solutions.logic.exercises;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogicExercisesApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogicExercisesApplication.class, args);
	}

}
